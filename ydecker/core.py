import urllib, os
import requests as req
import parsers as p

from bs4 import BeautifulSoup
from operator import itemgetter


def req_card(url):
    src = urllib.parse.urljoin("http://yugioh.wikia.com", url)
    res = req.get(src)

    if res.ok:
        parser = BeautifulSoup(res.text, 'html.parser')
        return parser
    else:
        return None

def is_token(tr):
    tag = tr.select_one("[title='Token Monster']")
    return tag

def is_card(tr):
    link = tr.find("a")
    return not is_token(tr) and link

def card_td(tr):
    return tr.select_one("td a:nth-of-type(2)")

def card_name(tr):
    if tr:    
        return tr['title']
    else:
        return None
    
def id_deck(dom):
    return [p.card_id(dom), p.which_deck(dom)]

def download_cards(dom):
    lines = dom.select_one(".wikitable").find_all("tr")
    cards_td = [card_td(x) for x in lines if is_card(x)]
    urls = map(itemgetter('href'), cards_td)

    return list(map(req_card, urls))

def group_cards(cards):
    m = {'main': [], 'extra': []}

    for (id, deck) in map(id_deck, cards):
        m[deck].append(id)

    return m

def build_deck(cards):
    main  = str.join("\n", cards['main'])
    extra = str.join("\n", cards['extra'])

    return str.join("\n", ['#main', main, '#extra', extra])

def build_and_save(deck_url, name):
    res = req.get(deck_url)

    if not res.ok:
        print("Resuest error. Please check the deck url")
        return None;

    soup = BeautifulSoup(res.text, 'html.parser')
    cards = download_cards(soup)
    content = build_deck( group_cards(cards) )

    os.makedirs("decks", exist_ok=True)
    path = "decks/{}.ydk".format(name)

    file = open(path, 'w')
    file.write(content)
    file.close()

    print("%s constructed!" % name)
    return None
