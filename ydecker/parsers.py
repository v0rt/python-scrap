from bs4 import BeautifulSoup

def card_id(dom):
    x = dom.select_one("[href='/wiki/Passcode']")
    return x.parent.next_sibling.a.string

def card_name(dom):
    th = dom.select_one(".cardtable th")
    return th.contents[0]

def is_monster(dom):
    tag = dom.select_one(".cardtablerowheader [href='/wiki/Card_type']")

    return tag.parent.next_sibling.a.string == 'Monster'

def is_xyz(dom):
    return dom.select_one("[title='Rank']")

def card_types(dom):
    if not is_monster(dom):
        return []

    td = dom.select_one("[href='/wiki/Type']")
    xs = td.parent.next_sibling.find_all('a')
    return [x.string for x in xs]

def which_deck(dom):
    extra = {"Synchro", "Xyz", "Fusion"}
    types = card_types(dom)

    if extra & frozenset(types):
        return 'extra'
    else:
        return 'main'
